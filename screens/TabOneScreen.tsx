import * as React from 'react';
import { StyleSheet, FlatList, TextInput, KeyboardAvoidingView } from 'react-native';

import EditScreenInfo from '../components/EditScreenInfo';
import { Text, View } from '../components/Themed';

const messages = [
  { text: 'Test1' },
  { text: 'Test2' },
  { text: 'Test3' },
  { text: 'Test4' },
  { text: 'Test5' },
  { text: 'Test6' },
  { text: 'Test7' },
  { text: 'Test8' },
  { text: 'Test9' },
  { text: 'Test10' },
]

export default class TabOneScreen extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      text: ''
    }
  }

  renderMessage = ({ item }) => {
    return (
      <View style={{ height: 80, alignItems: 'center' }}>
        <Text style={styles.title}>{item.text}</Text>
      </View>
    );
  }

  renderHeader = () => {
    return (
      <View style={{ height: 100 }}>
        <Text style={styles.title}>Header</Text>
      </View>
    );
  }

  renderFooter = () => {
    return (
      <View style={{ height: 100 }}>
        <Text style={styles.title}>Footer</Text>
        <TextInput
          style={styles.input}
          onChangeText={(text) => this.setState({ text })}
          value={this.state.text}
          placeholder={'Test'}
          underlineColorAndroid="transparent"
          multiline
          scrollEnabled={false}
        />
      </View>
    );
  }

  render = () => {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "position" : "height"}
        style={styles.container}
        keyboardVerticalOffset={180}
      >
          <FlatList
            ref={(ref) => { this.list = ref; }}
            keyboardShouldPersistTaps="handled"
            data={messages}
            keyExtractor={(item) => item.text.toString()}
            ListHeaderComponent={this.renderHeader}
            ListFooterComponent={this.renderFooter}
            renderItem={this.renderMessage}
            style={{ backgroundColor: 'blue' }}
            contentContainerStyle={{ backgroundColor: 'blue' }}
          />
      </KeyboardAvoidingView>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'blue',
    padding: 5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  input: {
    flex: 1,
    borderRadius: 3,
    borderWidth: 0.5,
    borderColor: '#999',
    padding: 10,
    margin: 20,
    fontSize: 16,
    paddingTop: 11,
  },
});
